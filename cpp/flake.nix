{
  description = "A C++ Flake using Zig!";

  outputs = { self, nixpkgs }: {

    packages.x86_64-linux.MyCppProject = (
        let
            system = "x86_64-linux";
            pkgs = nixpkgs.legacyPackages.${system};
        in
          pkgs.stdenv.mkDerivation {
              name = "MyCppProject";
              src = ./.;
              buildInputs = with pkgs; [
                zig
                fd
              ];
              phases = ["unpackPhase" "buildPhase" "installPhase"];
              buildPhase = ''
                export ZIG_GLOBAL_CACHE_DIR=/tmp
                fd --extension cpp --exec-batch zig c++ {}
              '';
              installPhase = ''
                mkdir -p $out/bin
                cp a.out $out/bin/MyCppProject
              '';
          }
        );

    defaultPackage.x86_64-linux = self.packages.x86_64-linux.MyCppProject;
  };
}

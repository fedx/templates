{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      with nixpkgs.legacyPackages.${system}; {
        packages.foo = rustPlatform.buildRustPackage {
          name = "foo";
          version = "0.1";
          buildInputs = [ cargo cmake rustc ];

          src = lib.cleanSource ./.;

          cargoSha256 = pkgs.lib.fakeHash;
        };

        defaultPackage = self.packages.${system}.foo;

        devShell = mkShell {
          inputsFrom = builtins.attrValues self.packages.${system};

          packages = [ 
            cargo
            rustc
            rustfmt
            cmake
            clippy
          ];
          nativeBuildInputs = [ cargo ];
          shellHook = " ls Cargo.toml || cargo init; ls Cargo.lock || cargo update";
        };
      });
}
